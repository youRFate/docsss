# docsss - s3 document organizer

Process scanned PDFs with [OCRmyPDF](https://github.com/jbarlow83/OCRmyPDF),
categorize them by contained keywords and upload them to an s3 compatible
object storage. This is inteded to run on a server that receives scans from
a network scanner. All the different operations are configurable using a toml
config file.

This is still very much work in progress and NOT ready for use.

# Requirements

* [boto3](https://pypi.org/project/boto3/)
* [toml](https://pypi.org/project/toml/)
* [OCRmyPDF](https://github.com/jbarlow83/OCRmyPDF)

# License

docsss is available freely under the MIT license.

All third party components incorporated into docsss are licensed under the
original license provided by the owner of the applicable component.
