from setuptools import setup, find_packages

setup(
    name="docsss",
    version="1.4.1",
    packages=find_packages(),
    entry_points={"console_scripts": ["docsss = docsss.__main__:cli"]},
    # Project uses reStructuredText, so ensure that the docutils get
    # installed or upgraded on the target machine
    author="Christoph Paa",
    description="docsss - s3 document organizer",
    url="https://gitlab.com/youRFate/docsss",
    install_requires=[
        "boto3",
        "ocrmypdf",
        "toml",
        "click",
        "paramiko",
    ],
    classifiers=["License :: OSI Approved :: Python Software Foundation License"],
)
