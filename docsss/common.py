from pathlib import Path
import os
import click
import boto3
import pprint
import paramiko
import abc

def get_remote(conf):
    class_map = {
        sc.remote_type : sc for sc in Client.__subclasses__()
    }
    remote_type = conf["remote_type"]
    specific_conf = conf[remote_type]
    subclass = class_map[remote_type]
    inst = subclass(specific_conf)
    return inst

class Client(metaclass=abc.ABCMeta):

    remote_type = "base"

    @abc.abstractmethod
    def __init__(self, conf):
        pass

    @abc.abstractmethod
    def upload(self, file_name, remote_folder):
        pass

    @abc.abstractmethod
    def download(self, remote_file, local_target):
        pass

    @abc.abstractmethod
    def delete(self, remote_file):
        pass

    @abc.abstractmethod
    def list_folder(self, remote_folder) -> list[str]:
        pass


class SftpClient(Client):
    remote_type = "sftp"

    def __init__(self, conf):
        print("sftp client created.")
        self.ssh_client = paramiko.SSHClient()
        self.ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        self.ssh_client.connect(
            conf["host"],
            username=conf["user"],
            port=conf["port"],
            key_filename=conf["key_filename"],
        )
        self.sftp_client = self.ssh_client.open_sftp()
        self.sftp_client.chdir(conf["base_folder"])

    def upload(self, file_name, remote_folder):
        resp = self.sftp_client.put(
            str(file_name),
            str(Path(remote_folder) / file_name.name)
        )

    def download(self, remote_file, local_target):
        resp = self.sftp_client.get(
            str(remote_file),
            str(local_target),
        )

    def delete(self, remote_file):
        self.sftp_client.remove(remote_file)

    def list_folder(self, remote_folder) -> list[str]:
        resp = self.sftp_client.listdir(remote_folder)
        return resp

class S3Client(Client):
    remote_type = "s3"

    def __init__(self, conf):
        print("s3 client created")
        self.s3 = boto3.client(
            "s3",
            endpoint_url=conf["endpoint"],
            aws_access_key_id=conf["access_key_id"],
            aws_secret_access_key=conf["secret_access_key"],
        )
        self.bucket = conf["bucket"]

    def upload(self, file_name, remote_folder):
        object_name = os.path.join(remote_folder, file_name.name)
        response = self.s3.upload_file(str(file_name), self.bucket, object_name)
        pprint.pprint(response)

    def download(self, remote_file, local_target):
        self.s3.download_file(self.bucket, remote_file, local_target)

    def delete(self, remote_file):
        self.s3.delete_object(Bucket=self.bucket, Key=remote_file)

    def list_folder(self, remote_folder) -> list[str]:
        # Gets a list of files in a folder on s3
        response = self.s3.list_objects_v2(
            Bucket=self.bucket,
            Prefix=remote_folder,
        )
        if response["IsTruncated"]:
            print("Too many files, rerun after completion!!")
        files = []
        for obj in response["Contents"]:
            if obj["Size"] > 0:
                # Only show files, not dirs
                files.append(obj["Key"])
        return files
