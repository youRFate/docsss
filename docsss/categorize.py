from pathlib import Path
import click
import subprocess
from . import common
import pprint
import tqdm
import tempfile
import os
import signal
from pprint import pprint


def query_user(file_attrs, conf) -> dict:
    # shows the file to the user, queries category, returns dict
    # this func does not change anything on the files, only gathers user input!
    attrs = file_attrs
    print("\nProcessing file {}".format(attrs["local_file"]))
    choices = list(conf["keywords"])
    choices.append("discard")
    choices.append("skip")
    choices = click.Choice(choices, case_sensitive=False)
    # show PDF
    proc = subprocess.Popen(
        "xdg-open {}".format(attrs["local_file"]), shell=True, preexec_fn=os.setsid
    )
    attrs["Category"] = click.prompt("Please categorize the file:", type=choices)
    # get file name suffix if the file is to be recategorized
    if not attrs["Category"] in ["discard", "skip"]:
        attrs["Suffix"] = click.prompt(
            "Append suffix to name", type=str, default=""
        ).replace(" ", "_")
    else:
        attrs["Suffix"] = ""
    # stop pdf viewer
    os.killpg(os.getpgid(proc.pid), signal.SIGTERM)
    # ask user to confirm
    print("Please review:")
    pprint(attrs)
    if click.confirm("Keep changes and continue?", default=True):
        return attrs
    else:
        # call again with original input
        return query_user(file_attrs, conf)


def apply_suffix(file_attrs) -> dict:
    # applies a suffix after the stem before the extension
    if file_attrs["Suffix"] == "":
        # noop
        return file_attrs
    old_path = file_attrs["local_file"]
    new_path = old_path.with_name(
        old_path.stem + "_" + file_attrs["Suffix"] + old_path.suffix
    )
    old_path.rename(new_path)
    file_attrs["local_file"] = new_path
    return file_attrs


def complete_s3_actions(file_attrs, rm: common.Client, no_delete=True) -> dict:
    # uploads files to new destination, if so configured. Deletes old file.
    # upload file to new location if category changed
    if not file_attrs["Category"] in ("skip", "discard"):
        # upload!
        rm.upload(file_attrs["local_file"], file_attrs["Category"])
    # Delete original file in s3, if skip wasn't selected
    if not file_attrs["Category"] == "skip" and not no_delete:
        rm.delete(file_attrs["remote_file"])
    return file_attrs


@click.command()
@click.option("--folder", type=str, help="Which folder to categorize")
@click.option("--no-delete", is_flag=True, help="Don't delete anything in s3")
@click.pass_context
def categorize(ctx, folder, no_delete):
    """
    Manually view and categorize PDF files.
    By default downloads all PDFs from the default folder in the config
    and shows them to the user one by one, prompting the user to categorize
    the file. then uploads all files to the specified folders.
    """
    conf = dict(ctx.obj)
    if folder is None:
        folder = conf["s3"]["default_folder"]
    # create client
    rm = common.get_remote(conf)
    # get folder list
    files = rm.list_folder(folder)
    file_map = []
    print(
        "Found {} files in folder '{}', starting download.".format(len(files), folder)
    )
    with tempfile.TemporaryDirectory() as tmpdirname:
        downloaded_files = []
        # download files to temp dir
        for idx in tqdm.tqdm(range(len(files)), desc="Downloading..."):
            file_map[idx]["remote_file"] = files[idx]
            file_map[idx]["local_file"] = Path(tmpdirname, Path(files[idx]).name)
            rm.download(
                file_map[idx]["remote_file"],
                str(file_map[idx]["local_file"]),
            )
        # prompt user for each file
        file_map = [query_user(f, conf) for f in file_map]
        # now do what the user asked us to do.
        # apply new suffix and move files
        file_map = [apply_suffix(f) for f in file_map]
        # process the changes in s3
        file_map = [complete_s3_actions(f, rm, no_delete) for f in file_map]
