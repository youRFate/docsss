#!/usr/bin/env python3

from pathlib import Path
import toml
import click
from . import ocr
from . import common
from . import categorize


def check_paths(toml_conf):
    toml_paths = toml_conf.get("paths")
    required_paths = ["scans_dir", "work_dir", "archive_dir"]
    # check if they are all there
    for path in required_paths:
        if path not in toml_paths.keys():
            raise Exception("Missing path from config file: {}".format(path))
    # sanitize any garbage the user might have input and check if they exist,
    # create them if they dont
    for key, value in toml_paths.items():
        p = Path(value).expanduser()
        toml_paths[key] = p
        if not p.exists():
            p.mkdir(parents=True)
    return toml_paths


# script that will do a bunch of stuff with scanned PDFs
@click.group()
@click.option(
    "--config-file",
    default="~/.docsss/docsss.toml",
    help="custom path to the config toml",
)
@click.pass_context
def cli(ctx, config_file):
    ctx.ensure_object(dict)
    # let's get our bearings
    conf = toml.load(Path(config_file).expanduser())
    # process paths.
    conf["paths"] = check_paths(conf)
    ctx.obj.update(conf)


cli.add_command(ocr.ocr)
cli.add_command(categorize.categorize)

if __name__ == "__main__":
    cli()
