from pathlib import Path
import os
from datetime import datetime
import click
import ocrmypdf
import shutil

from . import common


def ocr_pdf(file_name, ocr_conf):
    # uses ocrmypdf to add ocr stuff to the pdf
    # create output file name by nameing it old-name_ocr.pdf
    ocr_name = file_name.with_name(file_name.stem + "_ocr" + file_name.suffix)
    txt_name = ocr_name.with_suffix(".txt")
    # join language list with '+' if more than one lang is supplied
    if isinstance(ocr_conf["langs"], str):
        langs = ocr_conf["langs"]
    else:
        langs = "+".join(ocr_conf["langs"])
    # run the call.
    ocr_status = ocrmypdf.ocr(
        file_name,
        ocr_name,
        language=langs,
        sidecar=txt_name,
        deskew=True,
        tesseract_timeout=300,
        force_ocr=True,
    )
    # check return status
    if ocr_status == 0 and os.path.exists(ocr_name):
        os.unlink(file_name)
        return [ocr_name, txt_name]
    else:
        raise Exception(
            "Something went wrong during OCR-ing of file {}".format(file_name)
        )
    return [file_name, None]


def match_keywords(txt_name, keyword_conf):
    # checks if any keywords are present within the textfile, returns
    # the apropriate s3 folder if a match is found.
    match_folder = None
    with open(txt_name) as f:
        contents = set(f.read().split())
        for folder, keywords in keyword_conf.items():
            if contents & set(keywords):
                # we have a winner
                match_folder = folder
                break
    return match_folder


def process_file(file_name, conf, no_ocr, no_remote, keep_txt):
    # move to processing folder so we don't pick it up again as it's transformed
    working_name = conf["paths"]["work_dir"] / (
        datetime.now().isoformat(timespec="seconds").replace(":", "") + ".pdf"
    )
    shutil.move(file_name, working_name)
    category_folder = None
    if ("ocr" in conf.keys()) and not no_ocr:
        # do the OCR-ing
        print("OCR-ing...                    ", end="", flush=True)
        [working_name, txt_name] = ocr_pdf(working_name, conf["ocr"])
        print("[DONE]")
        # check for keywords in the textfile
        category_folder = match_keywords(txt_name, conf["keywords"])
        if not keep_txt:
            os.unlink(txt_name)
    # set to default if no OCR was done or no matches were found
    if category_folder is None:
        category_folder = conf.get("default_folder", "Uncategorized")
    print("Categorized to folder {}".format(category_folder))
    if not no_remote:
        rm = common.get_remote(conf)
        print("Uploading ...            ", end="", flush=True)
        rm.upload(working_name, category_folder)
        print("[DONE]")
    dest_dir = Path(conf["paths"]["archive_dir"] / category_folder)
    dest_dir.mkdir(exist_ok=True, parents=True)
    shutil.move(working_name, Path(dest_dir / working_name.name))


@click.command()
@click.option("--no-ocr", is_flag=True, help="disable OCR even if config exists")
@click.option("--no-remote", is_flag=True, help="disables remote upload")
@click.option("--keep-txt", help="do not delete the OCR txt file", is_flag=True)
@click.pass_context
def ocr(ctx, no_ocr, no_remote, keep_txt):
    conf = dict(ctx.obj)
    inputfiles = os.listdir(conf["paths"]["scans_dir"])
    for f in inputfiles:
        f = conf["paths"]["scans_dir"] / f
        if Path(f).suffix.lower() == ".pdf":
            print("Processing file {}".format(Path(f).name))
            process_file(f, conf, no_ocr, no_remote, keep_txt)
